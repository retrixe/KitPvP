import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.8.10"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

group = "com.retrixe"
version = "2.0.0-beta.2"

description = "An extensible and highly customisable kitPvP plugin."

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(8))
    }
}

repositories {
    mavenCentral()
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots")
    maven(url = "https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven(url = "https://maven.enginehub.org/repo/")
    maven(url = "https://repo.extendedclip.com/content/repositories/placeholderapi/")
}

dependencies {
    implementation("net.kyori:adventure-api:4.14.0")
    implementation("net.kyori:adventure-platform-bukkit:4.3.0")
    compileOnly("org.spigotmc:spigot-api:1.13.2-R0.1-SNAPSHOT")
    compileOnly("com.sk89q.worldguard:worldguard-legacy:6.2")
    compileOnly("com.sk89q.worldguard:worldguard-bukkit:7.0.0")
    compileOnly("me.clip:placeholderapi:2.11.3")
    // implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.10")
    // implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2"
}

// Necessary for Adventure to work correctly. We should probably relocate Kotlin stdlib too.
tasks.withType<ShadowJar> {
    relocate("net.kyori", "com.retrixe.kitpvp.net.kyori")
}
