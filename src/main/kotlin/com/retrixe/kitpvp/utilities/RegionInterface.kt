package com.retrixe.kitpvp.utilities

import com.retrixe.kitpvp.KitPvP
import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.bukkit.WorldGuardPlugin
import org.bukkit.Location

class RegionInterface(private val plugin: KitPvP) {
    private fun isWorldGuard7(): Boolean {
        return try {
            Class.forName("com.sk89q.worldguard.bukkit.RegionContainer")
            false
        } catch (e: ClassNotFoundException) {
            true
        }
    }

    fun isLocationInRegion(location: Location, regionName: String?): Boolean {
        val x = location.x
        val y = location.y
        val z = location.z
        // Check if the region is registered in KitPvP.
        val kitpvpRegion = plugin.config.regions[regionName]
        if (kitpvpRegion != null) {
            val lx = kitpvpRegion.pos1.x.coerceAtLeast(kitpvpRegion.pos2.x)
            val sx = kitpvpRegion.pos1.x.coerceAtMost(kitpvpRegion.pos2.x)
            val ly = kitpvpRegion.pos1.y.coerceAtLeast(kitpvpRegion.pos2.y)
            val sy = kitpvpRegion.pos1.y.coerceAtMost(kitpvpRegion.pos2.y)
            val lz = kitpvpRegion.pos1.z.coerceAtLeast(kitpvpRegion.pos2.z)
            val sz = kitpvpRegion.pos1.z.coerceAtMost(kitpvpRegion.pos2.z)
            return (lx > x && x > sx) && (ly > y && y > sy) && (lz > z && z > sz)
        }

        // Fallback to WorldGuard.
        val region = if (isWorldGuard7()) {
            WorldGuard.getInstance()
                .platform
                .regionContainer[BukkitAdapter.adapt(plugin.config.world)]
                ?.getRegion(regionName)
        } else {
            WorldGuardPlugin.inst().regionContainer[plugin.config.world]?.getRegion(regionName)
        }
        if (region == null) {
            plugin.logger.severe(
                "Region $regionName was requested, but it doesn't exist in WorldGuard or KitPvP!")
        }
        return region != null && region.contains(x.toInt(), y.toInt(), z.toInt())
    }
}
