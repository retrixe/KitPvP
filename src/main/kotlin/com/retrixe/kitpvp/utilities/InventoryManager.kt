package com.retrixe.kitpvp.utilities

import com.retrixe.kitpvp.KitPvP
import org.bukkit.GameMode
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import java.io.File
import java.nio.file.Paths
import java.util.logging.Level

// TODO: What if the server is abruptly killed? Persist state please.
class InventoryManager(private val plugin: KitPvP, path: String) {
    private val path: String = Paths.get(path, "inventory").toAbsolutePath().toString()

    fun inventoryExists(p: Player): Boolean = File(path, p.name + ".yml").exists()

    fun save(p: Player): Boolean {
        return try {
            val c = YamlConfiguration()
            c["health"] = p.health
            c["gamemode"] = p.gameMode.toString()
            c["allowFlight"] = p.allowFlight
            c["activePotionEffects"] = p.activePotionEffects
            c["inventory.content"] = p.inventory.contents
            c.save(File(path, p.name + ".yml"))
            p.inventory.clear()
            p.activePotionEffects.forEach { p.removePotionEffect(it.type) }
            true
        } catch (e: Exception) {
            plugin.logger.log(Level.SEVERE, "Failed to save ${p.name}'s inventory!", e)
            false
        }
    }

    fun restore(p: Player): Boolean {
        return try {
            val c = YamlConfiguration.loadConfiguration(File(path, p.name + ".yml"))
            if (c["health"] is Double) {
                p.fireTicks = 0
                p.health = c["health"] as Double
            }
            if (c["gamemode"] is String) {
                try {
                    p.gameMode = GameMode.valueOf(c["gamemode"] as String)
                } catch (e: IllegalArgumentException) {
                    plugin.logger.log(Level.SEVERE, "Error restoring inventory parsing gamemode", e)
                }
            }
            if (c["allowFlight"] is Boolean) {
                p.allowFlight = c["allowFlight"] as Boolean
            }
            if (c["activePotionEffects"] is List<*>) {
                p.activePotionEffects.forEach { p.removePotionEffect(it.type) }
                val potionEffects = c["activePotionEffects"] as List<*>
                for (obj in potionEffects) {
                    if (obj is PotionEffect) p.addPotionEffect(obj)
                }
            }
            if (c["inventory.content"] is List<*>) {
                // Load inventory.
                val content = c["inventory.content"] as List<*>
                val contents = arrayListOf<ItemStack?>()
                for (obj in content) {
                    contents.add(if (obj is ItemStack) obj else null)
                }
                p.inventory.contents = contents.toTypedArray()

                // Delete inventory copy.
                File(path, p.name + ".yml").delete()
            } else {
                plugin.logger.severe("Failed to restore ${p.name}'s inventory!")
                false
            }
        } catch (e: Exception) {
            plugin.logger.log(Level.SEVERE, "Failed to restore ${p.name}'s inventory!", e)
            false
        }
    }

    fun giveItem(itemStack: ItemStack, p: Player): Boolean {
        return try {
            val c = YamlConfiguration.loadConfiguration(File(path, p.name + ".yml"))
            if (c["inventory.content"] is List<*>) {
                // Give person item.
                val content = c["inventory.content"] as List<*>
                val contents = arrayListOf<ItemStack?>()
                var given = false
                for (i in content.indices) {
                    val item = content[i]
                    when {
                        item is ItemStack -> {
                            if (!given && item == itemStack && item.amount < item.maxStackSize) {
                                item.amount = item.amount + 1
                                given = true
                            }
                            contents.add(item)
                        }
                        !given -> {
                            contents.add(itemStack)
                            given = true
                        }
                        else -> contents.add(null)
                    }
                }
                // Save it.
                c["inventory.content"] = contents.toTypedArray()
                c.save(File(path, p.name + ".yml"))
                if (!given) {
                    plugin.logger.warning("Failed to give ${p.name} their reward, as they had a full inventory. " +
                            "(${itemStack.type})")
                }
                given
            } else {
                plugin.logger.severe("Failed to give ${p.name} their reward! (${itemStack.type})")
                false
            }
        } catch (e: Exception) {
            plugin.logger.log(Level.SEVERE, "Failed to give ${p.name} their reward! (${itemStack.type})", e)
            false
        }
    }
}
