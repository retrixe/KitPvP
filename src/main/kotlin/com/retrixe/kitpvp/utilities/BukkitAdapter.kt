package com.retrixe.kitpvp.utilities

import org.bukkit.Material

val signs
    get() = listOf(
        // 1.16
        "CRIMSON_SIGN",
        "CRIMSON_WALL_SIGN",
        "WARPED_SIGN",
        "WARPED_WALL_SIGN",
        // 1.14 and 1.15
        "ACACIA_SIGN",
        "ACACIA_WALL_SIGN",
        "BIRCH_SIGN",
        "BIRCH_WALL_SIGN",
        "DARK_OAK_SIGN",
        "DARK_OAK_WALL_SIGN",
        "JUNGLE_SIGN",
        "JUNGLE_WALL_SIGN",
        "OAK_SIGN",
        "OAK_WALL_SIGN",
        "SPRUCE_SIGN",
        "SPRUCE_WALL_SIGN",
        // <= 1.13
        "SIGN",
        "WALL_SIGN",
        // <= 1.12
        "SIGN_POST"
    )
        .map {
            try {
                return@map Material.valueOf(it)
            } catch (e: IllegalArgumentException) {
                return@map null
            }
        }
        .filterNotNull()
