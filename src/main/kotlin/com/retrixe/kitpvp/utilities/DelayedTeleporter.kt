package com.retrixe.kitpvp.utilities

import com.retrixe.kitpvp.KitPvP
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByBlockEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.*
import java.util.UUID
import java.util.concurrent.CompletableFuture

class DelayedTeleporter private constructor(private val player: Player) : Listener {
    companion object {
        private val teleportAllowed = HashMap<UUID, Boolean>()

        fun teleport(player: Player, location: Location, delay: Int): CompletableFuture<Boolean> {
            val completableFuture = CompletableFuture<Boolean>()
            teleportAllowed[player.uniqueId] = true
            val listener: Listener = DelayedTeleporter(player)
            Bukkit.getServer().pluginManager.registerEvents(listener, KitPvP.instance)
            Bukkit.getServer().scheduler.runTaskLater(KitPvP.instance, Runnable {
                HandlerList.unregisterAll(listener)
                val allowed = teleportAllowed.remove(player.uniqueId)
                if (allowed == true) {
                    completableFuture.complete(player.teleport(location))
                } else completableFuture.complete(false)
            }, delay.toLong())
            return completableFuture
        }

        fun isTeleportScheduled(uuid: UUID): Boolean {
            return teleportAllowed.containsKey(uuid)
        }
    }

    @EventHandler
    fun onPlayerMove(event: PlayerMoveEvent) {
        if (event.player == player) {
            val from = event.from
            val to = event.to
            if (to != null && (from.blockX != to.blockX || from.blockY != to.blockY || from.blockZ != to.blockZ)) {
                teleportAllowed.replace(player.uniqueId, false)
            }
        }
    }

    @EventHandler
    fun onPlayerTeleport(event: PlayerTeleportEvent) {
        if (event.player == player) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onPlayerChangedWorld(event: PlayerChangedWorldEvent) {
        if (event.player == player) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onPlayerDeath(event: PlayerDeathEvent) {
        if (event.entity == player) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        if (event.player == player) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onPlayerKick(event: PlayerKickEvent) {
        if (event.player == player) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onEntityDamage(event: EntityDamageEvent) {
        if (event.entity is Player && player == event.entity) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onEntityDamageByBlock(event: EntityDamageByBlockEvent) {
        if (event.entity is Player && player == event.entity) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onEntityDamageByPlayer(event: EntityDamageByEntityEvent) {
        if (event.damager is Player &&
            event.entity is Player && player == event.entity
        ) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }

    @EventHandler
    fun onEntityDamageByMob(event: EntityDamageByEntityEvent) {
        if (event.damager !is Player &&
            event.entity is Player && player == event.entity
        ) {
            teleportAllowed.replace(player.uniqueId, false)
        }
    }
}
