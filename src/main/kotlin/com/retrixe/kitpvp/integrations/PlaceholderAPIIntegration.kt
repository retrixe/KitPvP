package com.retrixe.kitpvp.integrations

import com.retrixe.kitpvp.KitPvP
import me.clip.placeholderapi.expansion.PlaceholderExpansion
import org.bukkit.OfflinePlayer

class PlaceholderAPIIntegration(private val plugin: KitPvP) : PlaceholderExpansion() {
    override fun persist(): Boolean {
        return true
    }

    override fun canRegister(): Boolean {
        return true
    }

    override fun getAuthor(): String {
        return plugin.description.authors.joinToString(", ")
    }

    override fun getIdentifier(): String {
        return "kitpvp"
    }

    override fun getVersion(): String {
        return plugin.description.version
    }

    override fun onRequest(player: OfflinePlayer?, params: String): String? {
        if (params.startsWith("status_") && params.length > 7) {
            val arenaName = params.substring(7)
            val arena = plugin.arenaManager.getArena(arenaName) ?: return "Invalid arena \"$arenaName\""
            return if (!arena.enabled) "Disabled"
            else if (arena.inProgress) "In Progress"
            else if (arena.isFull()) "Full"
            else "Available"
        }

        if (params.startsWith("type_") && params.length > 5) {
            val arenaName = params.substring(5)
            val arena = plugin.arenaManager.getArena(arenaName) ?: return "Invalid arena \"$arenaName\""
            return arena.config.type.let { if (it.matches("^.*[0-9].*$".toRegex())) it else it.uppercase() }
        }

        if (params.startsWith("playercount_") && params.length > 12) {
            val arenaName = params.substring(12)
            val arena = plugin.arenaManager.getArena(arenaName) ?: return "Invalid arena \"$arenaName\""
            return arena.players.size.toString()
        }

        return null
    }
}
