package com.retrixe.kitpvp

import com.retrixe.kitpvp.arenas.ArenaListener
import com.retrixe.kitpvp.arenas.ArenaManager
import com.retrixe.kitpvp.config.Config
import com.retrixe.kitpvp.integrations.PlaceholderAPIIntegration
import com.retrixe.kitpvp.utilities.InventoryManager
import com.retrixe.kitpvp.utilities.RegionInterface
import net.kyori.adventure.platform.bukkit.BukkitAudiences
import org.bukkit.plugin.java.JavaPlugin

class KitPvP : JavaPlugin() {
    companion object {
        lateinit var instance: KitPvP
        private set
    }
    lateinit var config: Config
    private set
    lateinit var adventure: BukkitAudiences
    private set
    lateinit var arenaManager: ArenaManager
    private set
    lateinit var arenaListener: ArenaListener
    private set
    lateinit var regionInterface: RegionInterface
    private set
    lateinit var inventoryManager: InventoryManager
    private set

    override fun onLoad() { instance = this }

    override fun onEnable() {
        // Setup config.
        saveDefaultConfig()
        config = Config(this, getConfig())
        // Setup class instances.
        adventure = BukkitAudiences.create(this)
        arenaManager = ArenaManager(this)
        arenaListener = ArenaListener(this)
        regionInterface = RegionInterface(this)
        inventoryManager = InventoryManager(this, this.dataFolder.absolutePath)
        // Initialise all arenas.
        arenaManager.initialiseArenas()
        // Initialise PlaceholderAPI integration.
        if (server.pluginManager.getPlugin("PlaceholderAPI") != null) {
            logger.info("Detected PlaceholderAPI, registering integration.")
            PlaceholderAPIIntegration(this).register()
        }
        // Initialise command.
        val command = getCommand("kitpvp")
            ?: throw RuntimeException("Broken plugin.yml! This JAR may be corrupted.")
        val kitpvpCommand = KitPvPCommand(this)
        command.setExecutor(kitpvpCommand)
        command.tabCompleter = kitpvpCommand
        // Register ArenaListener.
        server.pluginManager.registerEvents(arenaListener, this)
    }

    override fun onDisable() {
        adventure.close()
        arenaManager.stopArenas()
        // ArenaListener and /kpvp will be cleared by Spigot itself.
    }
}
