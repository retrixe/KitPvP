package com.retrixe.kitpvp

import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.TextReplacementConfig
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor
import org.bukkit.entity.Player

class KitPvPCommand(private val plugin: KitPvP) : TabExecutor { // TODO: /kitpvp info <arena>
    private fun onHelpCommand(sender: CommandSender) {
        var component = Lang.PREFIX.append(Lang.HELP_PREFIX)
        val components = arrayListOf<Component>(Lang.HELP_HELP, Lang.HELP_LIST)
        if (sender.hasPermission("kitpvp.join.other"))
            components.add(Lang.HELP_JOIN_ADMIN)
        else if (sender.hasPermission("kitpvp.join.self"))
            components.add(Lang.HELP_JOIN)
        if (sender.hasPermission("kitpvp.exit.other"))
            components.add(Lang.HELP_EXIT_ADMIN)
        else if (sender.hasPermission("kitpvp.exit.self"))
            components.add(Lang.HELP_EXIT)
        if (sender.hasPermission("kitpvp.spectate.other"))
            components.add(Lang.HELP_SPECTATE_ADMIN)
        else if (sender.hasPermission("kitpvp.spectate.self"))
            components.add(Lang.HELP_SPECTATE)
        if (sender.hasPermission("kitpvp.tp.other"))
            components.add(Lang.HELP_TP_ADMIN)
        else if (sender.hasPermission("kitpvp.tp.self"))
            components.add(Lang.HELP_TP)
        if (sender.hasPermission("kitpvp.openarena")) components.add(Lang.HELP_OPEN)
        if (sender.hasPermission("kitpvp.closearena")) components.add(Lang.HELP_CLOSE)
        if (sender.hasPermission("kitpvp.forcestartarena")) components.add(Lang.HELP_FORCESTART)
        if (sender.hasPermission("kitpvp.stoparena")) components.add(Lang.HELP_STOP)
        if (sender.hasPermission("kitpvp.reload")) components.add(Lang.HELP_RELOAD)
        for (index in 1..components.size) {
            component = component.append(components[index - 1])
            if (index < components.size) component = component.append(Component.text("\n"))
        }
        plugin.adventure.sender(sender).sendMessage(component)
    }

    private fun onListArenasCommand(sender: CommandSender) {
        var component = Lang.PREFIX.append(Lang.LIST_PREFIX)
        // Iterate through arenas, generate a list with statuses and send them to the user.
        var index = 0
        for ((name, arena) in plugin.arenaManager.arenas) {
            if (sender.hasPermission("kitpvp.arena.$name")) {
                val type = arena.config.type.let { if (it.matches("^.*\\d.*$".toRegex())) it else it.uppercase() }
                val nameReplace = TextReplacementConfig.builder().matchLiteral("{0}").replacement(name)
                val typeReplace = TextReplacementConfig.builder().matchLiteral("{1}").replacement(type)
                val playerCount = arena.waitingPlayers.size + arena.players.size
                val status =
                    if (!arena.enabled) Component.text("Disabled!").color(NamedTextColor.DARK_RED)
                    else if (arena.inProgress) Component.text("In Progress!").color(NamedTextColor.RED)
                    else if (arena.isFull()) Component.text("Full! $playerCount players on.").color(NamedTextColor.RED)
                    else Component.text("Available ($playerCount players on)").color(NamedTextColor.AQUA)
                val statusReplace = TextReplacementConfig.builder().matchLiteral("{2}").replacement(status)
                component = component.append(Lang.LIST_ARENA_LINE
                    .replaceText(nameReplace.build())
                    .replaceText(typeReplace.build())
                    .replaceText(statusReplace.build())
                )
                if (index + 1 < plugin.arenaManager.arenas.size)
                    component = component.append(Component.text("\n"))
            }
            index++
        }
        plugin.adventure.sender(sender).sendMessage(component)
    }

    // kpvp join <arena> (player)
    private fun onJoinArenaCommand(sender: CommandSender, args: Array<String>) {
        // Check player argument.
        val player = if (args.size == 3) {
            plugin.server.getPlayer(args[2]) ?:
            return plugin.adventure.sender(sender).sendMessage(Lang.INVALID_PLAYER_NAME)
        } else {
            if (sender is Player) sender
            else return plugin.adventure.sender(sender).sendMessage(Lang.ONLY_PLAYER_CAN_RUN)
        }
        // Check if already in an arena or kit selection area.
        if (plugin.arenaManager.getPlayerCurrentArena(player) != null ||
            plugin.arenaManager.getPlayerCurrentWaitingArena(player) != null) {
            return plugin.adventure.sender(sender).sendMessage(
                if (args.size == 2 || player == sender) Lang.SELF_ALREADY_IN_ARENA else Lang.PLAYER_ALREADY_IN_ARENA
            )
        }
        // Check if arena doesn't exist, is full or is in progress.
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)
        if (arena.inProgress) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_ALREADY_IN_PROGRESS)
        else if (arena.isFull()) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_ALREADY_FULL)
        else if (!arena.enabled) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_IS_NOT_OPEN)
        // Save inventory and remove from spectator, if spectating.
        plugin.arenaManager.getPlayerCurrentSpectatingArena(player)?.let {
            plugin.inventoryManager.restore(player)
            plugin.arenaManager.getArena(it)!!.spectators.remove(player)
        }
        if (player.inventory.firstEmpty() == -1) {
            plugin.adventure.sender(sender).sendMessage(Lang.CONSIDER_CLEARING_INV)
        }
        if (!plugin.inventoryManager.save(player)) {
            return plugin.adventure.sender(sender).sendMessage(Lang.FAILED_TO_SAVE_INV)
        }
        // Join to arena.
        arena.joinArena(player)
    }

    // kpvp spectate <arena> (player)
    private fun onSpectateArenaCommand(sender: CommandSender, args: Array<String>) {
        // Check player argument.
        val player = if (args.size == 3) {
            plugin.server.getPlayer(args[2]) ?:
            return plugin.adventure.sender(sender).sendMessage(Lang.INVALID_PLAYER_NAME)
        } else {
            if (sender is Player) sender
            else return plugin.adventure.sender(sender).sendMessage(Lang.ONLY_PLAYER_CAN_RUN)
        }
        // Check if already in an arena or kit selection area.
        if (plugin.arenaManager.getPlayerCurrentArena(player) != null ||
            plugin.arenaManager.getPlayerCurrentWaitingArena(player) != null) {
            return plugin.adventure.sender(sender).sendMessage(
                if (args.size == 2 || player == sender) Lang.SELF_ALREADY_IN_ARENA else Lang.PLAYER_ALREADY_IN_ARENA
            )
        }
        // Check if arena doesn't exist, is full or is in progress.
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)
        // TODO: FFA is never in progress :/
        // if (!arena.inProgress) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_NOT_IN_PROGRESS)
        if (!arena.enabled) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_IS_NOT_OPEN)
        // Save inventory and remove from spectator, if already spectating another arena.
        plugin.arenaManager.getPlayerCurrentSpectatingArena(player)?.let {
            plugin.inventoryManager.restore(player)
            plugin.arenaManager.getArena(it)!!.spectators.remove(player)
        }
        if (!plugin.inventoryManager.save(player)) {
            return plugin.adventure.sender(sender).sendMessage(Lang.FAILED_TO_SAVE_INV)
        }
        arena.spectateArena(player)
    }

    // kpvp exit/leave (player)
    private fun onExitArenaCommand(sender: CommandSender, args: Array<String>) {
        // Check player argument.
        val player = if (args.size == 2) {
            plugin.server.getPlayer(args[1]) ?:
            return plugin.adventure.sender(sender).sendMessage(Lang.INVALID_PLAYER_NAME)
        } else {
            if (sender is Player) sender
            else return plugin.adventure.sender(sender).sendMessage(Lang.ONLY_PLAYER_CAN_RUN)
        }
        val isSelf = args.size == 1 || player == sender
        // Check if not in an arena or kit selection area.
        val arenaName = plugin.arenaManager.getPlayerCurrentArena(player)
            ?: plugin.arenaManager.getPlayerCurrentWaitingArena(player)
            ?: plugin.arenaManager.getPlayerCurrentSpectatingArena(player)
            ?: return plugin.adventure.sender(sender).sendMessage(
                if (isSelf) Lang.SELF_NOT_IN_PVP else Lang.PLAYER_NOT_IN_PVP
            )
        // Exit from arena.
        plugin.arenaManager.getArena(arenaName)?.leaveArena(player, isSelf) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)
    }

    // kpvp tp (player)
    private fun onTpArenaCommand(sender: CommandSender, args: Array<String>) {
        // Check player argument.
        val player = if (args.size == 2) {
            plugin.server.getPlayer(args[1]) ?:
            return plugin.adventure.sender(sender).sendMessage(Lang.INVALID_PLAYER_NAME)
        } else {
            if (sender is Player) sender
            else return plugin.adventure.sender(sender).sendMessage(Lang.ONLY_PLAYER_CAN_RUN)
        }
        // Check if already in an arena or kit selection area.
        if (plugin.arenaManager.getPlayerCurrentArena(player) != null) {
            return plugin.adventure.sender(sender).sendMessage(
                if (player == sender) Lang.SELF_ALREADY_IN_ARENA else Lang.PLAYER_ALREADY_IN_ARENA
            )
        }
        val arena = plugin.arenaManager.getPlayerCurrentWaitingArena(player)?.let { plugin.arenaManager.getArena(it) }
            ?: return plugin.adventure.sender(sender).sendMessage(
                if (player == sender) Lang.SELF_NOT_IN_KIT_AREA else Lang.PLAYER_NOT_IN_KIT_AREA
            )
        // Check if kit is equipped or not.
        if (player.inventory.contents.none { it != null }) {
            return plugin.adventure.sender(sender).sendMessage(Lang.EQUIP_KIT_FIRST)
        }
        // Join to arena.
        arena.tpPlayer(player)
    }

    // kpvp open <arena>
    private fun onOpenArenaCommand(sender: CommandSender, args: Array<String>) {
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)

        if (arena.enabled) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_ALREADY_OPEN)
        arena.openArena()

        return plugin.adventure.sender(sender).sendMessage(Lang.OPENED_ARENA.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(args[1]).build()
        ))
    }

    // kpvp close <arena>
    private fun onCloseArenaCommand(sender: CommandSender, args: Array<String>) {
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)

        if (!arena.enabled) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_ALREADY_CLOSED)
        arena.closeArena()

        return plugin.adventure.sender(sender).sendMessage(Lang.CLOSED_ARENA.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(args[1]).build()
        ))
    }

    // kpvp forcestart <arena>
    private fun onForceStartArenaCommand(sender: CommandSender, args: Array<String>) {
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)

        if (arena.inProgress) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_ALREADY_IN_PROGRESS)
        arena.forceStartArena()

        return plugin.adventure.sender(sender).sendMessage(Lang.FORCESTARTED_ARENA.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(args[1]).build()
        ))
    }

    // kpvp stop <arena>
    private fun onStopArenaCommand(sender: CommandSender, args: Array<String>) {
        val arena = plugin.arenaManager.getArena(args[1]) ?:
        return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_DOES_NOT_EXIST)

        if (!arena.inProgress) return plugin.adventure.sender(sender).sendMessage(Lang.ARENA_NOT_IN_PROGRESS)
        arena.stopArena()

        return plugin.adventure.sender(sender).sendMessage(Lang.STOPPED_ARENA.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(args[1]).build()
        ))
    }

    private fun onReloadCommand(sender: CommandSender) {
        plugin.reloadConfig()
        plugin.arenaManager.stopArenas()
        plugin.config.reloadConfig(plugin.getConfig())
        plugin.arenaManager.reloadArenas()
        plugin.adventure.sender(sender).sendMessage(Lang.RELOAD)
    }

    override fun onCommand(
        sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (args.isEmpty() || args[0] == "help") {
            onHelpCommand(sender)
        } else if (args[0] == "list" || args[0] == "listarenas") {
            onListArenasCommand(sender)
        } else if (args[0] == "join" || args[0] == "spec" || args[0] == "spectate") { // <arena> (player)
            if (args.size != 2 && args.size != 3) {
                plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
                return true
            }
            val action = if (args[0] == "join") "join" else "spectate"
            val selfperm = sender.hasPermission("kitpvp.$action.self")
            val otherperm = sender.hasPermission("kitpvp.$action.other")
            if (args.size == 2 && !selfperm && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.$action.self").build()
                ))
            } else if (args.size == 3 && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.$action.other").build()
                ))
            } else if (!sender.hasPermission("kitpvp.arena.${args[1]}")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.arena.${args[1]}").build()
                ))
            } else if (args[0] == "join") onJoinArenaCommand(sender, args)
            else onSpectateArenaCommand(sender, args)
        } else if (args[0] == "exit" || args[0] == "leave") { // (player)
            if (args.size != 1 && args.size != 2) {
                plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
                return true
            }
            val selfperm = sender.hasPermission("kitpvp.exit.self")
            val otherperm = sender.hasPermission("kitpvp.exit.other")
            if (args.size == 1 && !selfperm && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.exit.self").build()
                ))
            } else if (args.size == 2 && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.exit.other").build()
                ))
            } else onExitArenaCommand(sender, args)
        } else if (args[0] == "tp") { // (player)
            if (args.size != 1 && args.size != 2) {
                plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
                return true
            }
            val selfperm = sender.hasPermission("kitpvp.tp.self")
            val otherperm = sender.hasPermission("kitpvp.tp.other")
            if (args.size == 1 && !selfperm && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.tp.self").build()
                ))
            } else if (args.size == 2 && !otherperm) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.tp.other").build()
                ))
            } else onTpArenaCommand(sender, args)
        } else if (args[0] == "open") { // <arena>
            if (args.size != 2) plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
            else if (!sender.hasPermission("kitpvp.openarena")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.openarena").build()
                ))
            } else onOpenArenaCommand(sender, args)
        } else if (args[0] == "close") { // <arena>
            if (args.size != 2) plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
            else if (!sender.hasPermission("kitpvp.closearena")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.closearena").build()
                ))
            } else onCloseArenaCommand(sender, args)
        } else if (args[0] == "forcestart") { // <arena>
            if (args.size != 2) plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
            else if (!sender.hasPermission("kitpvp.forcestartarena")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.forcestartarena").build()
                ))
            } else onForceStartArenaCommand(sender, args)
        } else if (args[0] == "stop") { // <arena>
            if (args.size != 2) plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
            else if (!sender.hasPermission("kitpvp.stoparena")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.stoparena").build()
                ))
            } else onStopArenaCommand(sender, args)
        } else if (args[0] == "reload") {
            if (args.size != 1) plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
            else if (!sender.hasPermission("kitpvp.reload")) {
                plugin.adventure.sender(sender).sendMessage(Lang.MISSING_PERMISSION.replaceText(
                    TextReplacementConfig.builder().once()
                        .matchLiteral("{0}").replacement("kitpvp.reload").build()
                ))
            } else onReloadCommand(sender)
        } else plugin.adventure.sender(sender).sendMessage(Lang.INVALID_USAGE)
        return true
    }

    private val subcommands =
        listOf("help", "list", "join", "spectate", "tp", "exit", "leave",
            "forcestart", "open", "close", "stop", "reload")

    private fun hasSubCommandPerm(sender: CommandSender, subcommand: String) =
        when (subcommand) {
            "help", "list" -> true
            "leave" -> sender.hasPermission("kitpvp.exit.self")
            "spec" -> sender.hasPermission("kitpvp.spectate.self")
            else -> sender.hasPermission("kitpvp.${subcommand}${when (subcommand) {
                "open", "close", "forcestart", "stop" -> "arena"
                "join", "tp", "spectate", "exit" -> ".self"
                else -> ""
            }}")
        }

    override fun onTabComplete(
        sender: CommandSender, command: Command, alias: String, args: Array<String>
    ): List<String> {
        val subCommandsWithArenaArg = arrayOf("join", "open", "close", "forcestart", "stop", "spec", "spectate")
        return when {
            args.isEmpty() -> subcommands
            args.size == 1 -> subcommands.filter { it.startsWith(args[0]) && hasSubCommandPerm(sender, it) }
            args.size == 2 && subCommandsWithArenaArg.contains(args[0]) -> {
                if (!hasSubCommandPerm(sender, args[0])) return listOf()
                return plugin.arenaManager.arenas.keys.filter {
                    it.startsWith(args[1]) && sender.hasPermission("kitpvp.arena.$it")
                }
            }
            args.size == 2 && (args[0] == "exit" || args[0] == "leave" || args[0] == "tp") -> {
                if (!hasSubCommandPerm(sender, args[0])) return listOf()
                return plugin.server.onlinePlayers.mapNotNull { if (it.name.startsWith(args[1])) it.name else null }
            }
            args.size == 3 && (args[0] == "join" || args[0] == "spec" || args[0] == "spectate") -> {
                if (!hasSubCommandPerm(sender, args[0])) return listOf()
                return plugin.server.onlinePlayers.mapNotNull { if (it.name.startsWith(args[2])) it.name else null }
            }
            else -> emptyList()
        }
    }
}
