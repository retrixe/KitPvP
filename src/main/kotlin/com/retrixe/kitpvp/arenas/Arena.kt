package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.config.Config
import com.retrixe.kitpvp.KitPvP
import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.TextReplacementConfig
import org.bukkit.GameMode
import org.bukkit.entity.Player
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

abstract class Arena(protected val plugin: KitPvP, val config: Config.ArenaConfig) {
    var enabled = true
        private set
    var inProgress = false
        protected set

    val spectators = arrayListOf<Player>()
    val waitingPlayers = arrayListOf<Player>()
    val players = arrayListOf<Player>()

    fun openArena() {
        stopArena()
        initialiseArena()
        enabled = true
    }

    fun closeArena() {
        stopArena()
        enabled = false
    }

    open fun isFull() = false // Override as and when required for arena specific functionality.

    // Do not call these functions if the arena is disabled.

    // Functionality.
    abstract fun initialiseArena()
    abstract fun forceStartArena()
    abstract fun stopArena()
    abstract fun tpPlayer(player: Player)
    abstract fun joinArena(player: Player)
    open fun spectateArena(player: Player) {
        player.teleport(config.teleports.random())
        spectators.add(player)
        player.allowFlight = true
        player.addPotionEffect(
            PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false, true)
        )
        player.gameMode = GameMode.SPECTATOR
        plugin.adventure.sender(player).sendMessage(
            Lang.SPECTATING_ARENA.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(config.name).build()
        ))
    }
    abstract fun leaveArena(player: Player, delayTeleport: Boolean)

    // Listeners.
    open fun onEntityDamageByEntity(e: EntityDamageByEntityEvent) {}
    open fun onPlayerDeath(e: PlayerDeathEvent) {}
    open fun onPlayerQuit(e: PlayerQuitEvent) {}
}
