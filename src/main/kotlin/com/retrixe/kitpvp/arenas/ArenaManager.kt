package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.KitPvP
import org.bukkit.entity.Player

class ArenaManager(private val plugin: KitPvP) {
    val arenas: HashMap<String, Arena> = hashMapOf()

    fun stopArenas() {
        for (arena in arenas.entries) {
            arena.value.stopArena()
        }
    }

    fun reloadArenas() {
        stopArenas()
        arenas.clear()
        initialiseArenas()
    }

    fun initialiseArenas() {
        if (plugin.config.mode == "disabled") {
            plugin.logger.warning("The KitPvP plugin is in `disabled` mode! Not loading any arenas.")
            return
        }
        for (arenaConfig in plugin.config.arenas.entries) {
            if (arenaConfig.value.type == "ffa") {
                arenas[arenaConfig.key] = FfaArena(plugin, arenaConfig.value)
            } else if (arenaConfig.value.type.matches("\\d+v\\d+".toRegex())) {
                // 1v1/2v2/3v3
                val teamSizes: Array<String> = arenaConfig.value.type.split("v").toTypedArray()
                if (teamSizes[0] != teamSizes[1]) {
                    throw RuntimeException("Invalid arena unexpectedly passed config validation!")
                } else {
                    arenas[arenaConfig.key] = TeamsArena(plugin, arenaConfig.value)
                }
            } else throw RuntimeException("Invalid arena unexpectedly passed config validation!")
        }
    }

    fun getPlayerCurrentArena(player: Player): String? {
        for (arena in arenas.entries) {
            if (arena.value.players.contains(player)) {
                return arena.key
            }
        }
        return null
    }

    fun getPlayerCurrentWaitingArena(player: Player): String? {
        for (arena in arenas.entries) {
            if (arena.value.waitingPlayers.contains(player)) {
                return arena.key
            }
        }
        return null
    }

    fun getPlayerCurrentSpectatingArena(player: Player): String? {
        for (arena in arenas.entries) {
            if (arena.value.spectators.contains(player)) {
                return arena.key
            }
        }
        return null
    }

    fun getArena(arena: String): Arena? = arenas[arena]
}
