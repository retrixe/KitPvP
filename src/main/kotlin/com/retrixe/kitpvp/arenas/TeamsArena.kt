package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.config.Config
import com.retrixe.kitpvp.utilities.DelayedTeleporter
import com.retrixe.kitpvp.KitPvP
import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.TextReplacementConfig
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.entity.Player
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.scheduler.BukkitTask

/*
Use an List<List<UUID>> of length 2 to store the users on each team.
Create a task that runs every 60 seconds, at the end of the task, it starts the arena.
Prevent more than 2 people joining.
Whenever a player joins the arena, start that task. When arena starts/ends, stop it.
When either player dies/quits, the game is over and both players are notified.

2v2/3v3: How will it work? The same way and the teams will be grouped in order of how people join.
Also, it will disable friendly fire. It will also inform people what team they're on.
Add a scoreboard? (TODO)
*/
class TeamsArena(plugin: KitPvP, config: Config.ArenaConfig) : Arena(plugin, config) {
    private val teamSize = config.type.split("v".toRegex()).toTypedArray()[0].toInt()
    private val blueTeamColor = NamedTextColor.BLUE
    private val blueTeam = arrayListOf<Player>()
    private val redTeamColor = NamedTextColor.RED
    private val redTeam = arrayListOf<Player>()
    private var queueWatchTask: BukkitTask? = null

    private fun evaluateArenaStop() {
        if (blueTeam.size != 0 && redTeam.size != 0) return
        else if (!inProgress && waitingPlayers.size == 0) {
            stopQueueTask()
            return
        } else if (!inProgress) return
        val winnerTeamColor = if (blueTeam.size > 0) blueTeamColor else redTeamColor
        val winnerTeam = Component.text(if (blueTeam.size > 0) "Blue" else "Red").color(winnerTeamColor)
        stopQueueTask()
        players.forEach {
            // Teleport to spawn before restoring inventory.
            it.teleport(plugin.config.spawn)
            it.inventory.clear()
            if (!plugin.inventoryManager.restore(it)) {
                plugin.adventure.sender(it).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
            if (teamSize > 1) {
                plugin.adventure.sender(it).sendMessage(
                    Lang.TEAM_WON.replaceText(
                        TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(winnerTeam).build()
                    )
                )
            }
        }
        blueTeam.clear()
        redTeam.clear()
        players.clear()
        inProgress = false
    }

    override fun isFull(): Boolean = waitingPlayers.size >= (teamSize * 2)

    override fun stopArena() { // Complete.
        stopQueueTask()
        players.forEach {
            // Teleport to spawn before restoring inventory.
            it.inventory.clear()
            it.teleport(plugin.config.spawn)
            if (!plugin.inventoryManager.restore(it)) {
                plugin.adventure.sender(it).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
            plugin.adventure.sender(it).sendMessage(Lang.ARENA_WAS_STOPPED)
        }
        players.clear()
        waitingPlayers.forEach {
            waitingPlayers.remove(it)
            // Teleport to spawn before restoring inventory.
            it.teleport(plugin.config.spawn)
            it.inventory.clear()
            if (!plugin.inventoryManager.restore(it)) {
                plugin.adventure.sender(it).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
            plugin.adventure.sender(it).sendMessage(Lang.ARENA_WAS_STOPPED)
        }
        waitingPlayers.clear()
        blueTeam.clear()
        redTeam.clear()
    }

    private fun startQueueTask() {
        if (queueWatchTask?.isCancelled == false) {
            plugin.logger.warning("Queue task of ${config.name} was attempted to start a second time.")
            return
        }
        var timeElapsed = 1
        queueWatchTask = plugin.server.scheduler.runTaskTimer(plugin, Runnable {
            val timeLeft = 20 - timeElapsed
            val blueNotReady = blueTeam.filter { it.inventory.contents.filterNotNull().isEmpty() }
            val redNotReady = redTeam.filter { it.inventory.contents.filterNotNull().isEmpty() }
            val anyPlayerNotReady = blueNotReady.isNotEmpty() || redNotReady.isNotEmpty()
            // If not enough players...
            if (waitingPlayers.size < (teamSize * 2)) {
                if (timeElapsed % 5 == 0) {
                    timeElapsed = 1
                    val neededPlayers = (teamSize * 2) - waitingPlayers.size
                    val replacement = TextReplacementConfig.builder().once().matchLiteral("{0}")
                        .replacement(Component.text(neededPlayers)).build()
                    for (player in waitingPlayers) {
                        plugin.adventure.sender(player).sendMessage(Lang.NOT_ENOUGH_PLAYERS.replaceText(replacement))
                    }
                } else timeElapsed++
                return@Runnable
            } else if (!anyPlayerNotReady && timeElapsed >= 20) {
                timeElapsed = 0 // For safety.
                return@Runnable forceStartArena()
            } else if (anyPlayerNotReady && timeElapsed >= 20) {
                timeElapsed = 1
                for (player in waitingPlayers) {
                    plugin.adventure.sender(player).sendMessage(Lang.KICKED_IDLE_PLAYERS)
                }
                blueNotReady.forEach { leaveArena(it, false) }
                redNotReady.forEach { leaveArena(it, false) }
                return@Runnable
            } else if (timeLeft % 5 == 0 || timeLeft < 5) {
                val seconds = TextReplacementConfig.builder().once().matchLiteral("{0}")
                    .replacement(Component.text(timeLeft)).build()
                for (player in waitingPlayers) {
                    plugin.adventure.sender(player).sendMessage(Lang.WAITING_PLAYERS_TO_EQUIP_KIT.replaceText(seconds))
                }
            }
            timeElapsed++
        }, 20, 20) // Delay 20 tick, at 0 tick, timeElapsed = 0, then at 20 tick, =1.
    }

    private fun stopQueueTask() {
        if (queueWatchTask?.isCancelled != true) queueWatchTask?.cancel()
    }

    override fun onPlayerDeath(e: PlayerDeathEvent) {
        // Check if killer was player.
        val killer = e.entity.killer
        if (config.rewards != null && killer != null) {
            rewardOnKill(killer, plugin, config)
        }
        // Remove player from team.
        var entityTeam = blueTeamColor
        if (!blueTeam.remove(e.entity)) {
            redTeam.remove(e.entity)
            entityTeam = redTeamColor
        }
        if (killer != null) {
            val killerTeam = if (entityTeam == blueTeamColor) redTeamColor else redTeamColor
            plugin.adventure.sender(killer).sendMessage(
                Lang.KILLED_PLAYER.replaceText(
                    TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(
                        Component.text(e.entity.name).color(entityTeam)
                    ).build()
                )
            )
            plugin.adventure.sender(e.entity).sendMessage(
                Lang.WAS_KILLED_BY_PLAYER.replaceText(
                    TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(
                        Component.text(killer.name).color(killerTeam)
                    ).build()
                )
            )
        } else plugin.adventure.sender(e.entity).sendMessage(Lang.WAS_KILLED_BY_UNKNOWN)
        evaluateArenaStop()
    }

    override fun onEntityDamageByEntity(e: EntityDamageByEntityEvent) {
        if (blueTeam.contains(e.damager) && blueTeam.contains(e.entity)) e.isCancelled = true
        else if (redTeam.contains(e.damager) && redTeam.contains(e.entity)) e.isCancelled = true
    }

    override fun onPlayerQuit(e: PlayerQuitEvent) {
        // Remove player from team.
        if (!blueTeam.remove(e.player)) redTeam.remove(e.player)
        // Evaluate if the arena needs to be stopped.
        evaluateArenaStop()
    }

    override fun initialiseArena() {} // Nothing special.

    override fun forceStartArena() {
        inProgress = true
        stopQueueTask()
        for (player in waitingPlayers) {
            joinToArena(player)
        }
        waitingPlayers.clear()
    }

    private fun joinToArena(player: Player) {
        player.teleport(config.teleports.random())
        player.health = 20.0
        player.fireTicks = 0
        // waitingPlayers.remove(player) - Handle outside this function.
        players.add(player)
        val blueTeam = Component.text(blueTeam.joinToString(", ") { it.name }).color(NamedTextColor.AQUA)
        val redTeam = Component.text(redTeam.joinToString(", ") { it.name }).color(NamedTextColor.LIGHT_PURPLE)
        plugin.adventure.sender(player).sendMessage(Lang.ARENA_HAS_STARTED.replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(blueTeam).build()
        ).replaceText(
            TextReplacementConfig.builder().once().matchLiteral("{1}").replacement(redTeam).build()
        ))
    }

    override fun tpPlayer(player: Player) {
        plugin.adventure.sender(player).sendMessage(Lang.TP_COMMAND_UNSUPPORTED)
    }

    override fun joinArena(player: Player) {
        player.teleport(config.kitselTp)
        waitingPlayers.add(player)
        plugin.adventure.sender(player).sendMessage(Lang.JOINED_ARENA)
        if (blueTeam.size >= teamSize) {
            redTeam.add(player)
            if (teamSize > 1) {
                plugin.adventure.sender(player).sendMessage(Lang.PLAYER_PART_OF_TEAM.replaceText(
                    TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(
                        Component.text("Red").color(redTeamColor)
                    ).build()
                ))
            }
        } else {
            blueTeam.add(player)
            if (teamSize > 1) {
                plugin.adventure.sender(player).sendMessage(Lang.PLAYER_PART_OF_TEAM.replaceText(
                    TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(
                        Component.text("Blue").color(blueTeamColor)
                    ).build()
                ))
            }
        }
        stopQueueTask() // Restart the queue when a new person joins.
        startQueueTask()
    }

    override fun leaveArena(player: Player, delayTeleport: Boolean) {
        if (inProgress) {
            return plugin.adventure.sender(player).sendMessage(Lang.CANNOT_LEAVE_ARENA_IN_PROGRESS)
        } else if (waitingPlayers.remove(player) || spectators.remove(player) || !delayTeleport) {
            if (!player.teleport(plugin.config.spawn))
                return plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_FAIL)
            if (!plugin.inventoryManager.restore(player))
                plugin.adventure.sender(player).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            players.remove(player)
            if (!blueTeam.remove(player)) redTeam.remove(player)
            evaluateArenaStop()
            return plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_SUCCESS)
        }
        // Begin delayed teleport.
        plugin.adventure.sender(player).sendMessage(
            Lang.EXIT_PRE.replaceText(
                TextReplacementConfig.builder()
                    .once().matchLiteral("{0}").replacement(plugin.config.tpDelay.toString()).build()
            )
        )
        DelayedTeleporter.teleport(player, plugin.config.spawn, plugin.config.tpDelay * 20).thenApply {
            // If successful, restore inventory.
            if (it) {
                player.fireTicks = 0
                if (!players.remove(player) || (!blueTeam.remove(player) && !redTeam.remove(player))) {
                    plugin.logger.severe("Failed to remove ${player.name} from ${config.name} arena's player list!")
                }
                evaluateArenaStop()
                if (!plugin.inventoryManager.restore(player)) {
                    plugin.adventure.sender(player).sendMessage(Lang.FAILED_TO_RESTORE_INV)
                } else plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_SUCCESS)
            } else plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_FAIL)
        }
    }
}
