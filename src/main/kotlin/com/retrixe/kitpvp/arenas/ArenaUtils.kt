package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.config.Config
import com.retrixe.kitpvp.KitPvP
import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.TextReplacementConfig
import org.bukkit.Material
import org.bukkit.block.Chest
import org.bukkit.entity.Player

fun rewardOnKill(killer: Player, plugin: KitPvP, config: Config.ArenaConfig) {
    if (config.rewards == null) return
    // Get the chest.
    val block = config.rewards.coords.block
    // If no chest, do this, else randomize rewards.
    if (block.type != Material.CHEST) {
        val coords = config.rewards.coords
        val worldName = if (coords.world?.name == null) "" else coords.world?.name + " "
        val loc = "${worldName}${coords.x.toInt()} ${coords.y.toInt()} ${coords.z.toInt()}"
        plugin.logger.severe("No chest at $loc for arena ${config.name}'s rewards!")
        plugin.adventure.sender(killer).sendMessage(Lang.INVALID_CONFIGURATION)
    } else {
        val random = Math.random() * 100
        val chest = block.state as Chest
        for (reward in config.rewards.rewards) {
            // These were pre-validated by Config.
            val chance = reward.split(" ").toTypedArray()[0].toDouble()
            val index = reward.split(" ").toTypedArray()[1].toInt() - 1
            val item = chest.inventory.getItem(index)
            if ( // Random chance hit and chest item is not null.
                random <= chance && item != null
            ) {
                val itemMeta = item.itemMeta
                val name = if (itemMeta?.hasDisplayName() == true) itemMeta.displayName
                else item.type.name.replace("_", " ").lowercase()
                if (!plugin.inventoryManager.giveItem(item, killer)) {
                    // Log stuff if it failed. The guy won a reward and couldn't get it, poor guy.
                    // TODO: fk the poor guy why didn't he clear his inv b4 joining (check inv manager too)
                    killer.sendMessage(
                        "§c[KitPvP] §4Your inventory is full! Contact an admin to get your reward: $name"
                    )
                    plugin.logger.info(
                        killer.name + " was unable to receive reward chest slot " +
                                reward.split(" ").toTypedArray()[1]
                    )
                } else plugin.adventure.sender(killer).sendMessage(
                    Lang.RECEIVED_REWARD.replaceText(
                        TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(name).build()
                    )
                )
            }
        }
    }
}
