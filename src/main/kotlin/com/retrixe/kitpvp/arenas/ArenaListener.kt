package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.utilities.signs
import com.retrixe.kitpvp.KitPvP
import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.TextReplacementConfig
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Chest
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.event.player.PlayerRespawnEvent
import org.bukkit.event.player.PlayerTeleportEvent
import org.bukkit.event.player.PlayerPortalEvent

class ArenaListener(private val plugin: KitPvP) : Listener {
    @EventHandler
    fun onPlayerQuitEvent(e: PlayerQuitEvent) {
        // Get the arena the user is in.
        val arenaName = plugin.arenaManager.getPlayerCurrentWaitingArena(e.player)
            ?: plugin.arenaManager.getPlayerCurrentSpectatingArena(e.player)
            ?: plugin.arenaManager.getPlayerCurrentArena(e.player) ?: return
        val arena = plugin.arenaManager.getArena(arenaName) ?: return
        // Remove from arena.
        arena.waitingPlayers.remove(e.player)
        arena.spectators.remove(e.player)
        arena.players.remove(e.player)
        // Teleport to spawn before restoring inventory.
        e.player.teleport(plugin.config.spawn)
        e.player.inventory.clear()
        plugin.inventoryManager.restore(e.player) // InventoryManager will log for us.
        // Pass event to arena.
        arena.onPlayerQuit(e)
    }

    @EventHandler
    fun onPlayerDeathEvent(e: PlayerDeathEvent) {
        // Get the arena the user is in.
        val arenaName = plugin.arenaManager.getPlayerCurrentWaitingArena(e.entity)
            ?: plugin.arenaManager.getPlayerCurrentArena(e.entity) ?: return
        val arena = plugin.arenaManager.getArena(arenaName) ?: return
        // Remove from arena.
        arena.waitingPlayers.remove(e.entity)
        arena.players.remove(e.entity)
        // Handle dead person (killer is handled by arena).
        e.keepInventory = true
        e.keepLevel = true
        e.droppedExp = 0
        e.drops.clear()
        // Inventory restoration and respawn location is handled using PlayerRespawnEvent.
        // Pass event to arena.
        arena.onPlayerDeath(e)
    }

    @EventHandler
    fun onPlayerRespawnEvent(e: PlayerRespawnEvent) {
        if (plugin.inventoryManager.inventoryExists(e.player)) {
            // Teleport to spawn before restoring inventory.
            e.respawnLocation = plugin.config.spawn
            e.player.inventory.clear()
            if (!plugin.inventoryManager.restore(e.player)) {
                plugin.adventure.sender(e.player).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
        }
    }

    @EventHandler
    fun onEntityDamageByEntityEvent(e: EntityDamageByEntityEvent) {
        val entity = e.entity
        if (entity !is Player) return
        // If a player is spectating, block damage to him.
        if (plugin.arenaManager.getPlayerCurrentSpectatingArena(entity) != null) {
            e.isCancelled = true
            return
        }
        // Get the arena the user is in.
        val arenaName = plugin.arenaManager.getPlayerCurrentWaitingArena(entity)
            ?: plugin.arenaManager.getPlayerCurrentArena(entity) ?: return
        val arena = plugin.arenaManager.getArena(arenaName) ?: return
        // Pass event to arena.
        arena.onEntityDamageByEntity(e)
    }

    @EventHandler
    fun onPlayerInteractEvent(e: PlayerInteractEvent) {
        // Get the arena the user is in.
        val arenaName = plugin.arenaManager.getPlayerCurrentWaitingArena(e.player) ?: return
        val arena = plugin.arenaManager.getArena(arenaName) ?: return
        // Handle the right click.
        val clickedSign = e.clickedBlock ?: return
        if (e.action == Action.RIGHT_CLICK_BLOCK &&
            signs.contains(clickedSign.type) &&
            arena.config.signs.any { clickedSign.location.zero() == it.zero() }
        ) {
            val sign = clickedSign.state as Sign
            val kitName = sign.getLine(1)
            var chestCoords: Location? = null
            for (kit in arena.config.kits) {
                if (kit.name == kitName) chestCoords = kit.coords
            }
            if (chestCoords == null) {
                val l = clickedSign.location
                val loc = "${l.x.toInt()} ${l.y.toInt()} ${l.z.toInt()}"
                plugin.logger.severe("Sign at $loc does not specify a valid kit!")
                plugin.adventure.sender(e.player).sendMessage(Lang.INVALID_CONFIGURATION)
                return
            }
            // Get the chests and set contents.
            val block = chestCoords.block
            if (block.type != Material.CHEST) {
                val loc = "${chestCoords.x.toInt()} ${chestCoords.y.toInt()} ${chestCoords.z.toInt()}"
                plugin.logger.severe("No chest at $loc for the kit $kitName specified in $arenaName arena!")
                plugin.adventure.sender(e.player).sendMessage(Lang.INVALID_CONFIGURATION)
                return
            }
            val chest = block.state as Chest
            if (chest.inventory.size != 54) {
                val loc = "${chestCoords.x.toInt()} ${chestCoords.y.toInt()} ${chestCoords.z.toInt()}"
                plugin.logger.severe("Chest at $loc for the kit $kitName specified in $arenaName arena is not a double chest!")
                plugin.adventure.sender(e.player).sendMessage(Lang.INVALID_CONFIGURATION)
                return
            }
            // Main inventory (1-3 row)
            for (i in 0..26) {
                e.player.inventory.setItem(i + 9, chest.inventory.getItem(i))
            }
            // Hotbar (4th row)
            for (i in 27..35) {
                e.player.inventory.setItem(i - 27, chest.inventory.getItem(i))
            }
            // Armor (5th row)
            e.player.inventory.helmet = chest.inventory.getItem(36)
            e.player.inventory.chestplate = chest.inventory.getItem(37)
            e.player.inventory.leggings = chest.inventory.getItem(38)
            e.player.inventory.boots = chest.inventory.getItem(39)
            // Item in off hand (6th row)
            e.player.inventory.setItemInOffHand(chest.inventory.getItem(45))
            plugin.adventure.sender(e.player).sendMessage(Lang.SELECTED_KIT.replaceText(
                TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(kitName).build()
            ))
        }
    }

    @EventHandler
    fun onPlayerMoveEvent(e: PlayerMoveEvent) {
        val arenaName = plugin.arenaManager.getPlayerCurrentSpectatingArena(e.player) ?: return
        val arena = plugin.arenaManager.getArena(arenaName)!!
        if (!plugin.regionInterface.isLocationInRegion(e.to ?: return, arena.config.name)) {
            e.isCancelled = true
            plugin.adventure.sender(e.player).sendMessage(Lang.CANNOT_LEAVE_ARENA_AS_SPECTATOR)
        }
    }

    @EventHandler
    fun onPlayerTeleportEvent(e: PlayerTeleportEvent) = onPlayerMoveEvent(e)

    @EventHandler
    fun onPlayerPortalEvent(e: PlayerPortalEvent) = onPlayerMoveEvent(e)
}
