package com.retrixe.kitpvp.arenas

import com.retrixe.kitpvp.config.Config
import com.retrixe.kitpvp.utilities.DelayedTeleporter
import com.retrixe.kitpvp.KitPvP
import com.retrixe.kitpvp.config.Lang
import net.kyori.adventure.text.TextReplacementConfig
import org.bukkit.entity.Player
import org.bukkit.event.entity.PlayerDeathEvent

class FfaArena(plugin: KitPvP, config: Config.ArenaConfig) : Arena(plugin, config) {
    override fun onPlayerDeath(e: PlayerDeathEvent) {
        // Check if killer was player.
        val killer = e.entity.killer
        if (config.rewards != null && killer != null) {
            rewardOnKill(killer, plugin, config)
        }
        if (killer != null) {
            plugin.adventure.sender(killer).sendMessage(Lang.KILLED_PLAYER.replaceText(
                TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(e.entity.name).build()
            ))
            plugin.adventure.sender(e.entity).sendMessage(Lang.WAS_KILLED_BY_PLAYER.replaceText(
                TextReplacementConfig.builder().once().matchLiteral("{0}").replacement(killer.name).build()
            ))
        } else plugin.adventure.sender(e.entity).sendMessage(Lang.WAS_KILLED_BY_UNKNOWN)
    }

    override fun initialiseArena() {} // Nothing special.
    override fun forceStartArena() {} // Nothing special.

    override fun stopArena() {
        players.forEach {
            // Teleport to spawn before restoring inventory.
            it.teleport(plugin.config.spawn)
            it.inventory.clear()
            if (!plugin.inventoryManager.restore(it)) {
                plugin.adventure.sender(it).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
            plugin.adventure.sender(it).sendMessage(Lang.ARENA_WAS_STOPPED)
        }
        players.clear()
        waitingPlayers.forEach {
            // Teleport to spawn before restoring inventory.
            it.teleport(plugin.config.spawn)
            it.inventory.clear()
            if (!plugin.inventoryManager.restore(it)) {
                plugin.adventure.sender(it).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            }
            plugin.adventure.sender(it).sendMessage(Lang.ARENA_WAS_STOPPED)
        }
        waitingPlayers.clear()
    }

    override fun tpPlayer(player: Player) {
        player.teleport(config.teleports.random())
        player.health = 20.0
        player.fireTicks = 0
        waitingPlayers.remove(player)
        players.add(player)
        plugin.adventure.sender(player).sendMessage(Lang.TPED_TO_ARENA)
    }

    override fun joinArena(player: Player) {
        player.teleport(config.kitselTp)
        waitingPlayers.add(player)
        plugin.adventure.sender(player).sendMessage(Lang.JOINED_ARENA)
    }

    override fun leaveArena(player: Player, delayTeleport: Boolean) {
        if (waitingPlayers.remove(player) || spectators.remove(player) || !delayTeleport) {
            if (!player.teleport(plugin.config.spawn))
                return plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_FAIL)
            if (!plugin.inventoryManager.restore(player))
                plugin.adventure.sender(player).sendMessage(Lang.FAILED_TO_RESTORE_INV)
            players.remove(player)
            return plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_SUCCESS)
        }
        // Begin delayed teleport.
        plugin.adventure.sender(player).sendMessage(
            Lang.EXIT_PRE.replaceText(TextReplacementConfig.builder()
                .once().matchLiteral("{0}").replacement(plugin.config.tpDelay.toString()).build())
        )
        DelayedTeleporter.teleport(player, plugin.config.spawn, plugin.config.tpDelay * 20).thenApply {
            // If successful, restore inventory.
            if (it) {
                player.fireTicks = 0
                if (!players.remove(player)) {
                    plugin.logger.severe("Failed to remove ${player.name} from ${config.name} arena's player list!")
                }
                if (!plugin.inventoryManager.restore(player)) {
                    plugin.adventure.sender(player).sendMessage(Lang.FAILED_TO_RESTORE_INV)
                } else plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_SUCCESS)
            } else plugin.adventure.sender(player).sendMessage(Lang.EXIT_POST_FAIL)
        }
    }
}
