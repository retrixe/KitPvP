package com.retrixe.kitpvp.config

import net.kyori.adventure.text.Component
import net.kyori.adventure.text.TextComponent
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextColor

// TODO: Ability to load lang.yml file with customisable helpCmd format.
object Lang {
    var PREFIX = Component.text("[KitPvP] ").color(TextColor.color(255, 0, 0))

    // Help command.
    var HELP_PREFIX = Component.text("Commands:\n").color(NamedTextColor.AQUA)
    private fun helpCmd(name: String, usage: String): TextComponent =
        Component.text(name).color(NamedTextColor.AQUA)
            .append(Component.text(" - ").color(NamedTextColor.RED))
            .append(Component.text(usage).color(NamedTextColor.AQUA))
    var HELP_HELP = helpCmd("/kpvp help", "Returns help information about /kpvp.")
    var HELP_LIST = helpCmd("/kpvp list", "List all available kitPvP arenas with their status.")
    var HELP_JOIN = helpCmd("/kpvp join <arena>", "Join a kitPvP arena.")
    var HELP_JOIN_ADMIN = helpCmd("/kpvp join <arena> (player)", "Join a kitPvP arena.")
    var HELP_SPECTATE = helpCmd("/kpvp spectate <arena>", "Spectate a kitPvP arena.")
    var HELP_SPECTATE_ADMIN = helpCmd("/kpvp spectate <arena> (player)", "Spectate a kitPvP arena.")
    var HELP_EXIT = helpCmd("/kpvp exit/leave", "Exit kitPvP arena.")
    var HELP_EXIT_ADMIN = helpCmd("/kpvp exit/leave (player)", "Exit kitPvP arena.")
    var HELP_TP = helpCmd("/kpvp tp", "Teleport from kit selection area to the arena.")
    var HELP_TP_ADMIN = helpCmd("/kpvp tp (player)",
        "Teleport from kit selection area to the arena.")
    var HELP_OPEN = helpCmd("/kpvp open <arena>", "Open an arena for players to join.")
    var HELP_CLOSE = helpCmd("/kpvp close <arena>", "Close an arena from players joining.")
    var HELP_FORCESTART = helpCmd("/kpvp forcestart <arena>",
        "Force a kitPvP arena to start, if possible.")
    var HELP_STOP = helpCmd("/kpvp stop <arena>", "Stop a kitPvP arena if in progress.")
    var HELP_RELOAD = helpCmd("/kpvp reload", "Reload the KitPvP plugin.")

    val LIST_PREFIX = Component.text("Available arenas:\n").color(NamedTextColor.AQUA)
    val LIST_ARENA_LINE = Component.text("{0} ({1})").color(NamedTextColor.AQUA)
        .append(Component.text(" - ").color(NamedTextColor.BLUE))
        .append(Component.text("{2}").color(NamedTextColor.AQUA))

    val OPENED_ARENA = PREFIX.append(Component.text("Opened {0} arena.").color(NamedTextColor.AQUA))

    val CLOSED_ARENA = PREFIX.append(Component.text("Closed {0} arena.").color(NamedTextColor.AQUA))

    val STOPPED_ARENA = PREFIX.append(Component.text("Stopped {0} arena.").color(NamedTextColor.AQUA))

    val FORCESTARTED_ARENA = PREFIX.append(Component.text("Forced {0} arena to start.").color(NamedTextColor.RED))

    var RELOAD = PREFIX
        .append(Component.text("Reloaded config successfully.").color(NamedTextColor.AQUA))

    val EXIT_PRE = PREFIX
        .append(Component.text("You will be teleported out of PvP in {0} seconds. ").color(NamedTextColor.AQUA))
        .append(Component.text("Do not move!").color(NamedTextColor.RED))
    val EXIT_POST_SUCCESS = PREFIX.append(Component.text("You have left PvP.").color(NamedTextColor.AQUA))
    val EXIT_POST_FAIL = PREFIX.append(Component.text("You moved from your position! Exit teleport cancelled.")
        .color(NamedTextColor.RED))

    val SELECTED_KIT = PREFIX
        .append(Component.text("You have selected ").color(NamedTextColor.AQUA))
        .append(Component.text("{0}").color(NamedTextColor.BLUE))
        .append(Component.text(" as your class.").color(NamedTextColor.AQUA))

    val EQUIP_KIT_FIRST = PREFIX
        .append(Component.text("Select a kit before joining the arena!").color(NamedTextColor.RED))

    val CONSIDER_CLEARING_INV = PREFIX
        .append(Component.text("[Warning] ").color(NamedTextColor.YELLOW))
        .append(Component.text("Your inventory is full! ").color(NamedTextColor.RED))
        .append(Component.text(
            "Consider leaving and clearing your inventory or you will be unable to get rewards."
        ).color(NamedTextColor.YELLOW))

    val SPECTATING_ARENA = PREFIX
        .append(Component.text("You are now spectating the ").color(NamedTextColor.AQUA))
        .append(Component.text("{0}").color(NamedTextColor.BLUE))
        .append(Component.text(" arena.").color(NamedTextColor.AQUA))
    val CANNOT_LEAVE_ARENA_AS_SPECTATOR = PREFIX
        .append(Component.text("You cannot leave the arena in spectator mode! Run ")
            .color(NamedTextColor.RED))
        .append(Component.text("/kpvp exit").color(NamedTextColor.AQUA))
        .append(Component.text(" to leave the arena.").color(NamedTextColor.RED))

    val JOINED_ARENA = PREFIX
        .append(Component.text("You have entered kit selection area.").color(NamedTextColor.AQUA))

    val TPED_TO_ARENA = PREFIX
        .append(Component.text("You have joined the kitPvP arena. Run ").color(NamedTextColor.AQUA))
        .append(Component.text("/kpvp exit").color(TextColor.color(255, 0, 0)))
        .append(Component.text(" to leave.").color(NamedTextColor.AQUA))

    val KILLED_PLAYER = PREFIX.append(Component.text("You killed ").color(NamedTextColor.AQUA))
        .append(Component.text("{0}").color(NamedTextColor.RED))
        .append(Component.text("!").color(NamedTextColor.AQUA))
    val WAS_KILLED_BY_UNKNOWN = PREFIX.append(Component.text("You were killed!").color(NamedTextColor.RED))
    val WAS_KILLED_BY_PLAYER = PREFIX.append(Component.text("You were killed by ").color(NamedTextColor.RED))
        .append(Component.text("{0}").color(NamedTextColor.AQUA))
        .append(Component.text("!").color(NamedTextColor.RED))
    val RECEIVED_REWARD = PREFIX.append(Component.text("You received a reward(!): ").color(NamedTextColor.AQUA))
        .append(Component.text("{0}").color(NamedTextColor.BLUE))

    // Teams arena messages.
    val TEAM_WON = PREFIX.append(Component.text("The {0} team won the match!").color(NamedTextColor.GREEN))
    val CANNOT_LEAVE_ARENA_IN_PROGRESS = PREFIX
        .append(Component.text("You cannot leave a teams match while it is in progress!").color(NamedTextColor.RED))
    val ARENA_HAS_STARTED = PREFIX
        .append(Component.text("The match has started!\n").color(NamedTextColor.LIGHT_PURPLE))
        .append(Component.text("Blue team: ").color(NamedTextColor.BLUE))
        .append(Component.text("{0}\n").color(NamedTextColor.AQUA))
        .append(Component.text("Red team: ").color(NamedTextColor.RED))
        .append(Component.text("{1}").color(NamedTextColor.LIGHT_PURPLE))
    val PLAYER_PART_OF_TEAM = PREFIX
        .append(Component.text("You are part of the {0} team!").color(NamedTextColor.GREEN))
    val NOT_ENOUGH_PLAYERS = PREFIX
        .append(Component.text("Not enough players to start. We need {0} more people.").color(NamedTextColor.RED))
    val WAITING_PLAYERS_TO_EQUIP_KIT = PREFIX
        .append(Component.text("Waiting {0} seconds for all players to equip a kit.").color(NamedTextColor.GOLD))
    val KICKED_IDLE_PLAYERS = PREFIX
        .append(Component.text("Removed all players who did not equip a kit.").color(TextColor.color(255, 0, 0)))
    val TP_COMMAND_UNSUPPORTED = PREFIX.append(Component.text("The ").color(NamedTextColor.RED))
        .append(Component.text("/kitpvp tp").color(NamedTextColor.AQUA))
        .append(Component.text(" command is not supported for this arena.").color(NamedTextColor.RED))

    // Various miscellaneous messages.
    var MISSING_PERMISSION = PREFIX.append(
        Component.text("You are missing the permission: ").color(NamedTextColor.RED)
    ).append(Component.text("{0}").color(NamedTextColor.AQUA))
    var INVALID_USAGE = PREFIX
        .append(Component.text("Invalid usage! Run ").color(NamedTextColor.RED)
            .append(Component.text("/kpvp help").color(NamedTextColor.AQUA))
            .append(Component.text(" for correct usage.").color(NamedTextColor.RED)))
    var ONLY_PLAYER_CAN_RUN = PREFIX
        .append(Component.text("Only players can run this command!").color(NamedTextColor.RED))
    var INVALID_PLAYER_NAME = PREFIX
        .append(Component.text("The specified player is not online!").color(NamedTextColor.RED))
    var PLAYER_ALREADY_IN_ARENA = PREFIX
        .append(Component.text("The specified player is already inside an arena! Leave before joining another.")
            .color(NamedTextColor.RED))
    var SELF_ALREADY_IN_ARENA = PREFIX
        .append(Component.text("You are already inside an arena! Leave before joining another.")
            .color(NamedTextColor.RED))
    var PLAYER_NOT_IN_PVP = PREFIX
        .append(Component.text("The specified player is not inside an arena or kit selection area!")
            .color(NamedTextColor.RED))
    var SELF_NOT_IN_PVP = PREFIX
        .append(Component.text("You are not inside an arena or kit selection area!").color(NamedTextColor.RED))
    var PLAYER_NOT_IN_KIT_AREA = PREFIX
        .append(Component.text("The specified player is not inside an arena's kit selection area!")
            .color(NamedTextColor.RED))
    var SELF_NOT_IN_KIT_AREA = PREFIX
        .append(Component.text("You are not inside an arena's kit selection area!").color(NamedTextColor.RED))
    var ARENA_DOES_NOT_EXIST = PREFIX
        .append(Component.text("The specified arena does not exist!").color(NamedTextColor.RED))
    var ARENA_NOT_IN_PROGRESS = PREFIX
        .append(Component.text("The specified arena is not in progress!").color(NamedTextColor.RED))
    var ARENA_ALREADY_IN_PROGRESS = PREFIX
        .append(Component.text("The specified arena is already in progress!").color(NamedTextColor.RED))
    var ARENA_ALREADY_CLOSED = PREFIX
        .append(Component.text("The specified arena is already closed to players!").color(NamedTextColor.RED))
    var ARENA_ALREADY_OPEN = PREFIX
        .append(Component.text("The specified arena is already open!").color(NamedTextColor.RED))
    var ARENA_ALREADY_FULL = PREFIX
        .append(Component.text("The specified arena is full!").color(NamedTextColor.RED))
    var ARENA_IS_NOT_OPEN = PREFIX
        .append(Component.text("The specified arena is not open!").color(NamedTextColor.RED))
    var ARENA_WAS_STOPPED = PREFIX
        .append(Component.text("The arena you were in was stopped.").color(NamedTextColor.RED))
    val FAILED_TO_SAVE_INV = PREFIX
        .append(Component.text("Failed to save your inventory!").color(NamedTextColor.RED))
    val FAILED_TO_RESTORE_INV = PREFIX
        .append(Component.text("Failed to restore your inventory!").color(NamedTextColor.RED))
    val INVALID_CONFIGURATION = PREFIX
        .append(Component.text("The plugin is configured incorrectly! Contact your server owner.")
            .color(NamedTextColor.RED))
}
