package com.retrixe.kitpvp.config

import com.retrixe.kitpvp.KitPvP
import org.bukkit.Location
import org.bukkit.World
import org.bukkit.configuration.file.FileConfiguration

class Config(private val plugin: KitPvP, config: FileConfiguration) {
    lateinit var mode: String
    lateinit var world: World
    lateinit var spawn: Location
    var tpDelay: Int = 0
    lateinit var regions: HashMap<String, Region>
    lateinit var arenas: HashMap<String, ArenaConfig>

    data class ArenaConfig(
        val name: String,
        val type: String,
        val kitsel: String,
        val kitselTp: Location,
        val kits: List<Kit>,
        val signs: List<Location>,
        val teleports: List<Location>,
        val rewards: ArenaRewards?)
    data class ArenaRewards(val coords: Location, val rewards: List<String>)
    data class Kit(val name: String, val coords: Location)
    data class Region(val pos1: Location, val pos2: Location)

    init { reloadConfig(config) }

    fun reloadConfig(config: FileConfiguration) {
        plugin.logger.info("Loading KitPvP config. Fix any issues with /kitpvp reload.")
        // Mode.
        mode = config.getString("mode") ?: "disabled"
        if (mode != "disabled" && mode != "multiple") {
            plugin.logger.severe("Invalid config setting: mode, defaulting to: disabled.")
            mode = "disabled"
        }
        // World.
        val worldName = config.getString("world") ?: "world"
        val bukkitWorld = plugin.server.getWorld(worldName)
        world = bukkitWorld ?: plugin.server.worlds[0]
        if (bukkitWorld == null) {
            plugin.logger.severe("Invalid config setting: world, defaulting to: ${world.name}")
        }
        // Spawn.
        val spawnStr = config.getString("spawn")
        spawn = if (spawnStr == null) {
            world.spawnLocation
        } else {
            try {
                parseLocationString(spawnStr)
            } catch (e: RuntimeException) {
                plugin.logger
                    .severe("Invalid config setting: spawn, defaulting to: world.spawnLocation")
                world.spawnLocation
            }
        }
        // Teleport delay.
        tpDelay = config.getInt("tpDelay", 5)
        // Regions.
        regions = hashMapOf()
        val regionConfig = config.getConfigurationSection("regions")
        if (regionConfig != null) {
            for (regionName in regionConfig.getKeys(false)) {
                val pos1 = regionConfig.getString("$regionName.pos1")
                val pos2 = regionConfig.getString("$regionName.pos2")
                if (pos1 == null || pos2 == null) {
                    plugin.logger.severe("Invalid region $regionName skipped (pos1/pos2 not provided)")
                    continue
                }
                try {
                    regions[regionName] = Region(
                        parseLocationString(pos1), parseLocationString(pos2)
                    )
                } catch (e: RuntimeException) {
                    plugin.logger.severe("Invalid region $regionName skipped (pos1/pos2 invalid)")
                    continue
                }
            }
        }
        // Arena.
        arenas = hashMapOf()
        val arenaConfig = config.getConfigurationSection("arenas")
        if (arenaConfig != null) {
            for (regionName in arenaConfig.getKeys(false)) {
                val type = arenaConfig.getString("$regionName.type")
                if (type == null || (type != "ffa" && !Regex("^\\d+v\\d+$").matches(type))) {
                    plugin.logger.severe("Invalid type $type for arena $regionName, arena skipped.")
                    continue
                }
                val kitsel = arenaConfig.getString("$regionName.kitsel")
                if (kitsel.isNullOrBlank()) {
                    plugin.logger.severe("Missing kit area for arena $regionName, arena skipped.")
                    continue
                }
                val kitselTpStr = arenaConfig.getString("$regionName.kitsel_tp")
                if (kitselTpStr.isNullOrBlank()) {
                    plugin.logger.severe(
                        "Missing kit area teleport for arena $regionName, arena skipped.")
                    continue
                }
                var kitselTp: Location
                try {
                    kitselTp = parseLocationString(kitselTpStr)
                } catch (e: NumberFormatException) {
                    plugin.logger.severe("Invalid kit area teleport $kitselTpStr for arena " +
                            "$regionName, arena skipped.")
                    continue
                }
                val kits = arrayListOf<Kit>()
                val kitsConfig = arenaConfig.getMapList("$regionName.kits")
                if (kitsConfig.isEmpty()) {
                    plugin.logger.warning("No kits are present in arena $regionName! " +
                            "However, proceeding without kits...")
                } else for (kit in kitsConfig) {
                    val name = kit["name"]
                    val coordsStr = kit["coords"]
                    if (name is String && coordsStr is String &&
                        name.isNotBlank() && coordsStr.isNotBlank()) {
                        try {
                            kits.add(Kit(name, parseLocationString(coordsStr)))
                        } catch (e: NumberFormatException) {
                            plugin.logger.warning("Kit coordinate $coordsStr ($name) in arena" +
                                    " $regionName is invalid! Skipping...")
                        }
                    } else {
                        plugin.logger.warning("A kit in arena $regionName is missing name/" +
                                "coords. Skipping it.")
                    }
                }
                val signs = arrayListOf<Location>()
                for (sign in arenaConfig.getStringList("$regionName.signs")) {
                    if (sign.isEmpty()) {
                        plugin.logger.warning("One of the signs in arena $regionName is empty. " +
                                "Skipping.")
                    } else {
                        try {
                            signs.add(parseLocationString(sign))
                        } catch (e: NumberFormatException) {
                            plugin.logger.warning("Sign $sign in arena $regionName is invalid. " +
                                    "Skipping.")
                        }
                    }
                }
                val teleports = arrayListOf<Location>()
                for (teleport in arenaConfig.getStringList("$regionName.teleports")) {
                    if (teleport.isEmpty()) {
                        plugin.logger.warning("One of the teleports in arena $regionName is " +
                                "empty. Skipping.")
                    } else {
                        try {
                            teleports.add(parseLocationString(teleport))
                        } catch (e: NumberFormatException) {
                            plugin.logger.warning("Teleport $teleport in arena $regionName is " +
                                    "invalid. Skipping.")
                        }
                    }
                }
                val rewards = arenaConfig.getConfigurationSection("$regionName.rewards")
                var arenaRewards: ArenaRewards? = null
                val rewardsCoords = rewards?.getString("coords")
                val rewardsChances = rewards?.getStringList("chances")
                if (rewards != null && rewardsChances != null && !rewardsCoords.isNullOrEmpty()) {
                    if (!rewardsChances.all {
                            val split = it.split(" ")
                            split.size == 2 && split[0].toDoubleOrNull() != null &&
                                    split[1].toDoubleOrNull() != null
                    }) {
                        plugin.logger.severe("Arena $regionName has some invalid reward chances!" +
                                " Rewards have not been loaded.")
                    } else arenaRewards = ArenaRewards(
                        parseLocationString(rewardsCoords), rewardsChances
                    )
                }
                arenas[regionName] = ArenaConfig(
                    regionName, type, kitsel, kitselTp, kits, signs, teleports, arenaRewards
                )
            }
        }
    }

    private fun parseLocationString(loc: String): Location {
        val split = loc.split(" ")
        if ((split.size != 3 && split.size != 5) || !split.all {
                try { it.toDouble(); true } catch (e: NumberFormatException) { false }
        }) throw RuntimeException("Invalid location string ($loc) passed to parseLocationString")

        val x = split[0].toDouble()
        val y = split[1].toDouble()
        val z = split[2].toDouble()
        return if (split.size == 3) Location(world, x, y, z)
        else Location(world, x, y, z, split[3].toFloat(), split[4].toFloat())
    }
}
